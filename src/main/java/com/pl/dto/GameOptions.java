package com.pl.dto;

import com.pl.enumarated.Difficulty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GameOptions {
    private int numberOfQuestions =1;
    private Difficulty difficulty;
    private int categoryId;
}
