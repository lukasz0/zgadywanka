package com.pl.dto;

import lombok.Data;

@Data
public class PlayerDto {

    private String name;
    private String password;
}
