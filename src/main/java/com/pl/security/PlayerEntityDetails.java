package com.pl.security;

import com.pl.database.entities.PlayerEntity;
import lombok.AllArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.stream.Collectors;

//klasa niezbedna do zmapowania w playerService z PlayerEntity na UserDetails bo loadUserByUsername zwraca UserDetails
@AllArgsConstructor
public class PlayerEntityDetails implements UserDetails {

    private final PlayerEntity entity;

    //musi zwórcić Collection<? extends GrantedAuthority> więc mapujemy do jakiejś prostej implementacji np SimpleGrantedAuthority
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return entity.getRoles()
                .stream()
                .map(x-> new SimpleGrantedAuthority(x))
                .collect(Collectors.toSet());
    }

    @Override
    public String getPassword() {
        return entity.getPassword();
    }

    @Override
    public String getUsername() {
        return entity.getName();
    }

    // dla poniższych metod napisać własną logikę (czy konto wygasło jest zablokowane itp)
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
