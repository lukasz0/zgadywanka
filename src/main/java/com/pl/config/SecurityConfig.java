package com.pl.config;

import com.pl.repositories.PlayerRepository;
import com.pl.services.PlayerDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private PlayerRepository playerRepository;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/addquestion").authenticated()
                .antMatchers("/").permitAll()
                .antMatchers("/h2/**").hasRole("Admin")
                .and()
                .formLogin()
                .permitAll()
                .defaultSuccessUrl("/index")
                .and()
                .logout()
                .and()
                .csrf().disable()
                .httpBasic();
    }

    //poniższe dwie metody sklejaja nam Spring security z klasą PlayerService. Klasa PlayerService teraz będzie sprawdzała czy uzytkownik jest w bazie czy go nie ma
    @Override
    protected void configure(final AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authenticationProvider());
    }

    @Bean
    public AuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        PlayerDetailsService playerDetailsService=new PlayerDetailsService(playerRepository);
        provider.setUserDetailsService(playerDetailsService);
        provider.setPasswordEncoder(passwordEncoder());//kodowanie hasla
        return provider;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
