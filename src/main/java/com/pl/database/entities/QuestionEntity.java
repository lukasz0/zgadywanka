package com.pl.database.entities;

import com.pl.enumarated.Difficulty;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="questions")
@NoArgsConstructor
@Getter
@ToString
public class QuestionEntity {

    @Id
    @GeneratedValue
    private Long idQuestion;
    //private String category;
    private String type;
    private Difficulty difficulty;
    private String question;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="question_id")
    private List<AnswerEntity> answers;

    public QuestionEntity(String type, Difficulty difficulty, String question, List<AnswerEntity> answers) {
        this.type = type;
        this.difficulty = difficulty;
        this.question = question;
        this.answers = answers;
    }
}
