package com.pl.database.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name="categories")
@NoArgsConstructor
@Getter
@ToString
public class CategoryEntity {

    @Id
    @GeneratedValue
    private Long id;
    private String name;

    @OneToMany(cascade= CascadeType.ALL)
    @JoinColumn(name="category_id")
    private final List<QuestionEntity> questionEntity=new ArrayList<>();

    public CategoryEntity(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public CategoryEntity(String name) {
        this.name = name;
    }

    public void addQuestion(QuestionEntity questionEntity) {
        this.questionEntity.add(questionEntity);
    }
}
