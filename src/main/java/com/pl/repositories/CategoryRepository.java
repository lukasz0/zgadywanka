package com.pl.repositories;

import com.pl.database.entities.CategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CategoryRepository extends JpaRepository<CategoryEntity,Long> {
    boolean existsCategoryEntityByName(String name);

    @Query("FROM CategoryEntity c WHERE upper(c.name) like upper(?1)")
    CategoryEntity findCategoryEntityByName(String category);

    @Query("select (count(c.id)>0) from CategoryEntity c where c.id=?1")
    boolean hasExistsCategoryWithID(Long categoryId);
}
