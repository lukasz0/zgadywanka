package com.pl.repositories;

import com.pl.database.entities.PlayerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PlayerRepository extends JpaRepository<PlayerEntity,Long> {

    Optional<PlayerEntity> findByNameIgnoreCase(String playerName);
}
