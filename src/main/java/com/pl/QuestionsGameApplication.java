package com.pl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuestionsGameApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuestionsGameApplication.class, args);
	}

}
