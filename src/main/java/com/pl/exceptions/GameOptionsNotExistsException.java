package com.pl.exceptions;

public class GameOptionsNotExistsException extends RuntimeException{
    public GameOptionsNotExistsException(String message){
        super(message);
    }
    public GameOptionsNotExistsException(String message, Throwable cause){
        super(message,cause);
    }
}
