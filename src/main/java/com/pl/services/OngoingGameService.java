package com.pl.services;


import com.pl.dto.GameOptions;
import com.pl.dto.QuestionsDto;
import com.pl.exceptions.GameOptionsNotExistsException;
import com.pl.enumarated.Difficulty;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;

import java.time.LocalDateTime;
import java.util.*;

@Service
@SessionScope
@Log
public class OngoingGameService {

    private GameOptions gameOptions;
    private int currentQuestionIndex;
    private int points;

    private List<QuestionsDto.QuestionDto> questions;
    private final QuizDataService quizDataService;
    private LocalDateTime startGame;
    private LocalDateTime endGame;
    private final List<QuestionsDto.QuestionDto> questionsWithCorrectAnswer = new ArrayList<>();
    private final List<QuestionsDto.QuestionDto> questionsWithIncorrectAnswer = new ArrayList<>();

    public OngoingGameService(QuizDataService quizDataService) {
        this.quizDataService = Objects.requireNonNull(quizDataService);
    }

    public void init(GameOptions gameOptions) {
        if (gameOptions==null || gameOptions.getDifficulty()==null){
            throw new GameOptionsNotExistsException("Game options or difficulty is null");
        } else {
            log.info("from init:" + gameOptions);
            this.gameOptions = gameOptions;
            this.currentQuestionIndex = 0;
            this.points = 0;
            this.questions = quizDataService.getQuizQuestions(gameOptions);
            this.startGame = LocalDateTime.now();
        }
    }

    public int getCurrentQuestionNumber() {
        return currentQuestionIndex + 1;
    }

    public String getCurrentQuestion() {
        QuestionsDto.QuestionDto dto = questions.get(currentQuestionIndex);
        return dto.getQuestion();
    }

    public int getTotalQuestionNumber() {
        return questions.size();
    }

    public List<String> getCurrentQuestionAnswersInRandomOrder() {
        QuestionsDto.QuestionDto questionDto = questions.get(currentQuestionIndex);
        List<String> answers = new ArrayList<>();
        answers.addAll(questionDto.getIncorrectAnswers());
        answers.add(questionDto.getCorrectAnswer());

        Collections.shuffle(answers); //tasuj - chcemy otrzymać odpowiedzi losowo
        return answers;
    }

    public boolean checkAnswerFromCurrentQuestionAndUpdatePoints(String userAnswer) {
        log.info("Correct answer: " + questions.get(currentQuestionIndex).getCorrectAnswer());
        log.info("Questions: " + questions.get(currentQuestionIndex).getQuestion());
        log.info("User answer: " + userAnswer);
        if (userAnswer.equals(questions.get(currentQuestionIndex).getCorrectAnswer())) {
            points++;
            questionsWithCorrectAnswer.add(questions.get(currentQuestionIndex));
            return true;
        }
        questionsWithIncorrectAnswer.add(questions.get(currentQuestionIndex));
        return false;
    }

    public boolean proceedToNextQuestion() {
        currentQuestionIndex++;
        if (currentQuestionIndex == questions.size()) {
            endGame = LocalDateTime.now();
            log.info("end game");
            addGame();
        }
        return currentQuestionIndex < questions.size();
    }

    public Difficulty getDifficulty() {
        return gameOptions.getDifficulty();
    }

    public String getCategoryName() {
        Optional<String> category = quizDataService.getQuizCategories().stream()
                .filter(categoryDto -> categoryDto.getId() == gameOptions.getCategoryId())
                .map(categoryDto -> categoryDto.getName())
                .findAny();
        return category.orElse(null);
    }

    public int getPoints() {
        return points;
    }

    public void addGame() {
        quizDataService.addGame(gameOptions.getDifficulty(), gameOptions.getCategoryId(), points, questions.size(), startGame, endGame, questionsWithCorrectAnswer, questionsWithIncorrectAnswer);
    }
}
