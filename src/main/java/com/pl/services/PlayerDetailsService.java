package com.pl.services;

import com.pl.repositories.PlayerRepository;
import com.pl.security.PlayerEntityDetails;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@AllArgsConstructor
public class PlayerDetailsService implements UserDetailsService {

    private final PlayerRepository playerRepository;

    @Override
    public UserDetails loadUserByUsername(String playerName) throws UsernameNotFoundException {
        return playerRepository.findByNameIgnoreCase(playerName)
                .map(x -> new PlayerEntityDetails(x))
                .orElseThrow(() -> new UsernameNotFoundException(playerName));
    }
}
