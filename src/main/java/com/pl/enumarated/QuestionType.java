package com.pl.enumarated;

public enum QuestionType {
    trueOrFalse,
    multiple,
    open
}
