package com.pl;

import com.pl.dto.QuestionsDto;
import com.pl.database.entities.CategoryEntity;
import com.pl.database.entities.PlayerEntity;
import com.pl.enumarated.Difficulty;
import com.pl.services.PlayerService;
import com.pl.services.QuizDataService;
import lombok.extern.java.Log;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import com.pl.repositories.PlayerRepository;

import java.util.List;


//Rozszerzanie interfejsu CommandLineRunner wymusza na nas dostarczenie implementacji
// metody run, która zostanie uruchomiona automatycznie po
// załadowaniu kontekstu aplikacji Springowej, zaraz przed zakończeniem wywołania metody run z maina
@Component
@Log
public class StartupRunner implements CommandLineRunner {

    private final PlayerRepository playerRepository;
    private final QuizDataService quizDataService;
    private final PlayerService playerService;

    public StartupRunner(PlayerRepository playerRepository, QuizDataService quizDataService, PlayerService playerService) {
        this.playerRepository = playerRepository;
        this.quizDataService = quizDataService;
        this.playerService = playerService;
    }

    @Override
    public void run(String ... args){
        log.info("Executing startup actions ...");

        playerService.addPlayer("Adam","aa");
        playerService.addPlayer("Jan","bb");
        playerService.addPlayer("Lukasz","cc");

        log.info("List of players from database: ");
        List<PlayerEntity> playerEntities = playerRepository.findAll();
        for (PlayerEntity playerEntity: playerEntities){
            log.info("Retrieved player: " + playerEntity);
        }
        CategoryEntity categoryEntity = quizDataService.addCategory("Mathemastics: Questions from DB");
        QuestionsDto.QuestionDto questionDto = new QuestionsDto.QuestionDto(Long.toString( categoryEntity.getId()), "multiple", Difficulty.medium,"Pierwsze pytanie to ....","prawidłowa odpoweidz", List.of("pierwsza błędna","druga błędna","trzecia błędna"));
        quizDataService.addQuestion(questionDto);
        quizDataService.getQuizCategories();
    }
}
